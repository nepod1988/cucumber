Feature: When I go to main page
  with clear cookies
  I want to check out pop up with cookies notifications
  and ability to log in

  Scenario: Checking out pop up with cookies and log ig
    Given browser with clear data
    And I open "http://me.mercedes-benz.com"
    When The page is loaded I want to see notification
    Then I close it by clicking on special button
    And choosing language by clicking on "Germany"