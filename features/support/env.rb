require 'cucumber/rails'

ActionController::Base.allow_rescue = false

begin
  require 'database_cleaner'
  require 'database_cleaner/cucumber'

  DatabaseCleaner.strategy = :truncation
rescue NameError
  raise "You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it."
end

#   Before('~@no-txn', '~@selenium', '~@culerity', '~@celerity', '~@javascript') do
#     DatabaseCleaner.strategy = :transaction
#   end
#
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, :browser => :firefox)

end

Capybara.javascript_driver = :selenium
Capybara.default_driver = :selenium

Cucumber::Rails::Database.javascript_strategy = :truncation

Before do |scenario|
  DatabaseCleaner.start
  load Rails.root.join('db/seeds.rb')
  Capybara.current_session.driver.browser.manage.delete_all_cookies
end

Before '@javascript' do
  page.driver.browser.manage.delete_all_cookies
  Capybara.current_session.driver.browser.manage.delete_all_cookies
end


After do |scenario|
  DatabaseCleaner.clean
end